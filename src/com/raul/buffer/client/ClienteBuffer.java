package com.raul.buffer.client;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que abstrai implementa��es de clientes do buffer
 */
public abstract class ClienteBuffer implements Runnable {

    /**
     * Objeto com a conex�o ao buffer
     */
    protected final Socket conexao;
    protected final String nome;

    /**
     * Constr�i um novo cliente do buffer
     *
     * @param ip IP do servidor de buffer
     * @param porta porta do servidor de buffer
     * @param id Identificador do produtor
     */
    public ClienteBuffer(String ip, int porta, int id) {
        Thread.currentThread().setName(getClass().getSimpleName() + id);
        this.nome = Thread.currentThread().getName();

        this.conexao = conectar(ip, porta);
    }

    /**
     * Conecta no buffer via socket
     *
     * @return boolean sucesso
     */
    private Socket conectar(String ip, int porta) {
        try {
            Socket socket = new Socket(ip, porta);
            return socket;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Desconecta do buffer
     */
    protected void desconectar() {
        try {
            conexao.close();
        } catch (IOException e) {
            System.out.println("Erro ao desconectar do servidor! " + nome);
        }
    }
}
